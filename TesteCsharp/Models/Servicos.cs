﻿using System;
using System.Collections.Generic;

namespace TesteCsharp.Models
{
    public partial class Servicos
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public DateTime DataAtendimento { get; set; }
        public decimal Valor { get; set; }
        public string Tipo { get; set; }
        public int? IdCliente { get; set; }
        public int? IdFornecedor { get; set; }

        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual Fornecedor IdFornecedorNavigation { get; set; }
    }
}
