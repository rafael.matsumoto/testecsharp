﻿using System;
using System.Collections.Generic;

namespace TesteCsharp.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            Servicos = new HashSet<Servicos>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }

        public virtual ICollection<Servicos> Servicos { get; set; }
    }
}
