﻿using System;
using System.Collections.Generic;

namespace TesteCsharp.Models
{
    public partial class Fornecedor
    {
        public Fornecedor()
        {
            Servicos = new HashSet<Servicos>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }

        public virtual AspNetUsers User { get; set; }
        public virtual ICollection<Servicos> Servicos { get; set; }
    }
}
